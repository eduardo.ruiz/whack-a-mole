import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';

import './styles.css';

import Hole from '../hole';
import Mole from '../mole';

const Terrain = (props) => {

  const _renderHoles = () => {
    return Object.values(props.holes).map((_hole, _i) => {
      const _mole = props.moles[_hole.mole];
      return (
        <Hole key={`hole_${_i}`}
          mole={ <Mole mole={_mole} onWhack={props.onWackMole} peepOut={_mole.isOut} /> }
        />
      );
    });
  };

  return (
    <div className="Terrain">
      { _renderHoles( props ) }
    </div>
  );
};


Terrain.propTypes = {
  holes: PropTypes.object,
  moles: PropTypes.object,
  onWackMole: PropTypes.func,
};

Terrain.defaultProps = {
  holes: {},
  moles: {},
  onWackMole: null,
};

export default Terrain;