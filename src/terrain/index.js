import React, { useState } from 'react';
import PropTypes from 'prop-types';


import { createNewMole, initializeGame, createNewGame, molesMovement, playGame, getMolePeepTime } from '../GameCore.js';
import Terrain from './Terrain';
import { statement } from '@babel/template';


const TerrainWrapper = (props) => {

  const game = createNewGame();
  initializeGame({ game, holes: props.holes });
  molesMovement({ game });

  const [isPlaying, setIsPlaying] = useState(false);
  const [holes, setHoles] = useState(game.holes);
  const [moles, setMoles] = useState(game.moles);

  const _onWackMole = (_data) => {
    console.log('Terrain -> _onWackMole');  // TODO: REMOVE DEBUG LOG
    console.log(_data);  // TODO: REMOVE DEBUG LOG
    game.shots++;
    game.score++;
    onUpdateScore({ game });
  };

  const _gameControl = () => {
    const _playstep = playGame({ game });

    if (_playstep.state === 'play' && _playstep.mole) {
      const _hiddenTime = getMolePeepTime({ mole: _playstep.mole });
      _playstep.mole.isOut = true;
      setTimeout(()=> { // Hide the mole
        _playstep.mole.isOut = false;
        _playstep.mole.isMoving = false;
        setMoles({ 
          ..._playstep.game.moles,
          [_playstep.mole.id]: _playstep.mole,
         });
      }, _hiddenTime);
    }

    setHoles( {..._playstep.game.holes} );
    setMoles( {..._playstep.game.moles} );

    if (_playstep.state !== 'end') {
      setTimeout( _gameControl, 500);
    }
  };

  if (!isPlaying) {
    setIsPlaying(true);
    setTimeout( _gameControl, 500 );  // Start game control after half a second
  }
  
  return (
    <Terrain holes={holes} moles={moles} onWackMole={_onWackMole}></Terrain>
  );

};


TerrainWrapper.propTypes = {
  holes: PropTypes.number,
  onUpdateScore: PropTypes.func,
};

TerrainWrapper.defaultProps = {
  holes: null,
  onUpdateScore: null,
};


export default TerrainWrapper;