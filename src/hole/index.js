import React, { useState, useContext } from 'react';

import Hole from './Hole';


const HoleWrapper = (props) => {
  return(
    <Hole mole={props.mole}>
    </Hole>
  );
};


HoleWrapper.propTypes = {};

HoleWrapper.defaultProps = {};

export default HoleWrapper;