import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';


import './styles.css';

const Hole = (props) => {
  return (
    <div className="Hole">
      {props.mole}
    </div>
  );
}




Hole.propTypes = {
  mole: PropTypes.object,
};

Hole.defaultProps = {
  mole: null,
};

export default Hole;

