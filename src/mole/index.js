import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Mole from './Mole';


const MoleWrapper = (props) => {

  const [peepOut, setPeepOut] = useState(props.peepOut);
  useEffect(() => {
    console.log('MoleWrapper -> useEffect for peepOut'); // TODO: REMOVE DEBUG LOG
    console.log(props.peepOut); // TODO: REMOVE DEBUG LOG
    setPeepOut(props.peepOut);
  },[props.peepOut]);

  return(
    <Mole
      peepOut={peepOut}
      onWhack={() => props.onWhack(props.mole)} 
    />
  );
};


MoleWrapper.propTypes = {
  mole: PropTypes.object,
  onWhack: PropTypes.func,
  peepOut: PropTypes.bool,
};


MoleWrapper.defaultProps = {
  mole: null,
  onWhack: null,
  peepOut: false,
};



export default MoleWrapper;