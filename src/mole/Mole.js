import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';

import './styles.css';


const Mole = (props) => {
  let _MoleStyle = `Mole`;
  _MoleStyle = props.peepOut ? `${_MoleStyle} Peep` : _MoleStyle; 

  return (
    <div className={_MoleStyle} onClick={props.onWhack}>
    </div>
  );

};


Mole.propTypes = {
  peepOut: PropTypes.bool,
  onWhack: PropTypes.func,
};

Mole.defaultProps = {
  peepOut: false,
  onWhack: null,
};


export default Mole;