import React, { useState } from 'react';
import PropTypes from 'prop-types';


import ScoreBoard from './ScoreBoard';

const ScoreBoardWrapper = (props) => {
  return (<ScoreBoard game={props.game}/>);
};


ScoreBoardWrapper.propTypes = {
};

ScoreBoardWrapper.defaultProps = {
};


export default ScoreBoardWrapper;