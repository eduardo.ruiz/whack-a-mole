import React, { useState } from 'react';
import PropTypes from 'prop-types';

const ScoreBoard = (props) => {

  const {game} = props;

  return (
    <div>
      <h1>Whack a mole</h1>
      <h2>Shots: ${game.shots}</h2>
      <h2>Hits: ${game.hits}</h2>
      <h2>Score: ${game.score}</h2>
    </div>
  );
};

ScoreBoard.propTypes = {
  game: PropTypes.object,
};

ScoreBoard.defaultProps = {
  game: {},
};


export default ScoreBoard;
